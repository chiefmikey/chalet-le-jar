#!/bin/sh

INSTANCE_ALREADY_STARTED="INSTANCE_ALREADY_STARTED_PLACEHOLDER"
if [ ! -e ~/$INSTANCE_ALREADY_STARTED ]; then
sudo touch ~/$INSTANCE_ALREADY_STARTED
  echo "-- First instance startup --"
    sudo yum update -y
    sudo yum install -y wget unzip git
    sudo rm -rf /usr/local/go
    wget -O go.tar.gz https://golang.org/dl/go1.17.2.linux-amd64.tar.gz
    sudo tar -C /usr/local -xzf go.tar.gz
    sudo rm go.tar.gz
    sudo echo "export PATH=$PATH:/usr/local/go/bin" >> ~/.profile
    source ~/.profile
    git clone https://github.com/coredns/coredns
    cd coredns
    make
    sudo echo \
      ".:53 {
        rewrite name exact mco.lbsg.net ip.chaletlejar.com
        forward . 1.1.1.1:53
      }" \
    >> ~/coredns/Corefile
    sudo chmod +x ~/coredns/coredns
    screen -S coredns -dm sudo ~/coredns/coredns
else
  echo "-- Not first instance startup --"
    sudo yum update -y
    screen -S coredns -dm sudo ~/coredns/coredns
fi
