## Ubuntu

```sh
sudo apt install -y wget
sudo wget -O ~/mc-server-init.sh https://raw.githubusercontent.com/chiefmikey/scripts/main/mc-server/mc-server-init.sh
sudo chmod +x ~/mc-server-init.sh
sudo ~/mc-server-init.sh
```

Resume screen with `screen -r`

Detach screen with `ctrl+a` then `d`
